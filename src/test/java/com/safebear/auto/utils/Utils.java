package com.safebear.auto.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class Utils {
    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");
    private static final String BROWSER = System.getProperty("browser", "chrome");


    public static String getUrl() {
        return URL;
    }

    public static WebDriver getDriver() {

        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
        System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe"); //ie does not close after the test


        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized", "incognito");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);

            case "firefox":
                return new FirefoxDriver();

            case "headless":
                options.addArguments("headless", "disable-gpu");
                return new ChromeDriver(options);

            case "ie":
                return new InternetExplorerDriver();

            default:
                return new ChromeDriver(options);
        }

    }

    public static String generateScreenShotFileName() {
        //create filename
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".png";
    }

    public static void capturescreenshot(WebDriver driver, String fileName) {

        //Take screenshot
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        //Make sure that the screenshots directory exists
        File file = new File("target/screenshots");
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }

        //Copy file to filename and location we set before
        try {
            copy(scrFile, new File("target/screenshots/" + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static WebElement waitForElement(By locator, WebDriver driver) {

        //This is where we set the timeout value (of 5 seconds)
        WebDriverWait wait = new WebDriverWait(driver, 5);

        WebElement element;

        try {

            //Try to find the element straight away
            element = driver.findElement(locator);

        } catch (TimeoutException e) {

            //If you can't, use the 'explicit wait'
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            element = driver.findElement(locator);
        } catch (Exception e) {

            //If it still isn't there, print a stack trace and take a screenshot
            System.out.println(e.toString());
            capturescreenshot(driver, generateScreenShotFileName());
            throw (e);

        }
        //return the web element if it's been found
        return element;
    }
}

