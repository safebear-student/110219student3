package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

import static com.safebear.auto.utils.Utils.waitForElement;

@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();

    @NonNull
    WebDriver driver;

    //CHECKS
    public String getPageTitle() {

        return driver.getTitle();
    }


    public String checkForFailedLoginWarning() {
        return waitForElement(locators.getLoginWarningLocator(), driver).getText();
    }

    //ACTIONS
    public void enterUsername(String username) {
        waitForElement(locators.getUsernameLocator(), driver).sendKeys(username);
    }

    public void enterPassword(String password) {
        waitForElement(locators.getPasswordLocator(), driver).sendKeys(password);
    }

    public void clickLoginButton() {
        waitForElement(locators.getLoginButonLocator(), driver).click();
    }
}
