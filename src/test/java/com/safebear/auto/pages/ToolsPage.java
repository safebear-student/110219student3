package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

import static com.safebear.auto.utils.Utils.waitForElement;

@RequiredArgsConstructor
public class ToolsPage {

    ToolsPageLocators locators = new ToolsPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String checkForLoginSuccessfulMessage(){
        return waitForElement(locators.getSuccessfulLoginMessageLocator(),driver).getText();
    }
}
