package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

/**
 * The @Data Lombok command is creating virtual getters and setters for the private variables.
 */
@Data
public class ToolsPageLocators {

    //Message locators
    private By successfulLoginMessageLocator = By.xpath(".//body/div[@class = 'container']/p/b");
}
