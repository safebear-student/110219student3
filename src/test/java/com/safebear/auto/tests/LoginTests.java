package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static com.safebear.auto.utils.Utils.getDriver;
import static org.testng.Assert.assertTrue;

public class LoginTests {

    private WebDriver driver = getDriver();
    LoginPage loginPage = new LoginPage(driver);
    ToolsPage toolsPage = new ToolsPage(driver);

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void validLogin(){

        //go to login page

        driver.get(Utils.getUrl());

        //enter valid details
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");

        //click on login
        loginPage.clickLoginButton();

        //assert message is correct
        assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Login Successful"));

    }

    @Test
    public void invalidLogin(){

        //go to login page

        driver.get(Utils.getUrl());

        //enter invalid details
        loginPage.enterUsername("tester");
        loginPage.enterPassword("blahblah");

        //click on login
        loginPage.clickLoginButton();

        //assert message is correct
        assertTrue(loginPage.checkForFailedLoginWarning().contains("Username or Password is incorrect"));

    }

}
