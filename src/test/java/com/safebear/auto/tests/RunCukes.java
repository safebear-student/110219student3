package com.safebear.auto.tests;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterClass;

@CucumberOptions(
        plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "json:target/cucumber-report.json"},
        tags = "not @to-do",
        glue = "com.safebear.auto.tests",
        features = "classpath:toolslist.features"
)
public class RunCukes extends AbstractTestNGCucumberTests {

//    @AfterClass
//    public static void writeExtentReport() {
//        Reporter.loadXMLConfig("src/extent-config.xml");
//    }
}