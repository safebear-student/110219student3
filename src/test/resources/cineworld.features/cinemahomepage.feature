Feature: Cineworld Homepage
  In order to decide what to watch at the cinema
  As a User
  I want to see the listings for my nearest cinema

  Rules:
  * The user must know if there's a cinema close to them
  * The user must be informed if there's no cinemas close to them

  Glossary:
  * User: Someone who wants to see cinema listings

  Scenario Outline: the user view's their nearest cinema
    Given I am in <myLocation>
    When I visit the cineworld website
    Then I can see the listings for <cinemalocation>
    Examples:
      | myLocation | cinemalocation          |
      | BS12 3CD   | Bristol                 |
      | LON12 3CD  | London Leicester Square |
      | AB12 3CD   | Aberdeen                |


  @HighRisk
  @HighImpact
  Scenario: the user has a cinema near to them
    Given I have a cinema close to me
    When I visit the cineworld website
    Then I can see the listings for my nearest cinema

  @HighRisk
  @HighImpact
  Scenario: the user does not have a cinema near to them
    Given I do not have a cinema close to me
    When I visit the cineworld website
    Then I can see the information message explaining I don't have a cinema near me


